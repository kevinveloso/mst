/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;

import java.util.ArrayList;
import mst.Heap;

public class Dijkstra {
        
    
    public void DijkstraPath(int[][] matrix, int[] vertices) {
        
        ArrayList<HeapNode> final_path = new ArrayList<HeapNode>();
        ArrayList<HeapNode> nodes = Heap.buildMinHeap(vertices);
       
        
        for(HeapNode node : nodes) {
            node.setValue(Integer.MAX_VALUE);
        }
        
        nodes.get(0).setValue(0);
                
        while(!nodes.isEmpty()) {
            
            HeapNode root = Heap.extractMin(nodes);
            
            final_path.add(root);
            
            for(HeapNode node : nodes){
              int[] adj = matrix[node.getPosition()];
                            
              for(int i = 0; i < adj.length; i++) {
                  
                  if(nodes.get(i).getValue() < adj[i] &&
                          nodes.get(i).getValue() != Integer.MAX_VALUE &&
                          nodes.get(i).getValue() != 0) {
                    nodes.get(i).setValue(nodes.get(i).getValue() + adj[i]);
                  }
                  
              }
              
            }
            
        }        
    }

}
