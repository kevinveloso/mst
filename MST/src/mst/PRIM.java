/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;


public class PRIM {
    
    // Numero de vertices
    private int vertices;

    public PRIM(int vertices) {
        this.vertices = vertices;
    }


        // Função utilizada para procurar o vertice com a chave de menor valor
    // Entre os vertices não incluidos na MST ainda
    public int minKey(int[] key, boolean[] mstSet) {

        int min = Integer.MAX_VALUE;
        int minIndex = -1;

        for (int v = 0; v < vertices; v++) {
            if (key[v] <= min && !mstSet[v]) {
                min = key[v];
                minIndex = v;
            }
        }

        return minIndex;
    }

   
    public void primMST(int[][] graph) {

        // Array para armazenar MST construida
        int parent[] = new int[vertices];

        // Valores das chaves, peso das arestas
        int key[] = new int[vertices];

        // Array para indicar se o vertice já foi adicionado a MST ou não
        boolean mstSet[] = new boolean[vertices];

        for (int i = 0; i < vertices; i++) {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        key[0] = 0;

        // O primeiro vertice não tem pai
        parent[0] = -1;

        // MST possui N-1 vetices
        for (int count = 0; count < vertices - 1; count++) {

            int u = minKey(key, mstSet);

            mstSet[u] = true;

            for (int v = 0; v < vertices; v++) {

                if (graph[u][v] != 0 && !mstSet[v] && graph[u][v] < key[v]) {

                    parent[v] = u;
                    key[v] = graph[u][v];
                }
            }
        }

        // Print MST construida
        printMST(parent, vertices, graph);
    }
    
     // Função utilizada para imprimir a MST armazenada em parent[]
    public void printMST(int parent[], int n, int[][] graph) {
        System.out.println("Peso da aresta");
        int total_cost=0;
        for (int i = 1; i < vertices; i++) {
            System.out.println(parent[i] + " - " + i + "  " + graph[i][parent[i]]);
            total_cost+=graph[i][parent[i]];
        }
        System.out.println("MST formado: "+total_cost);
    }

}
