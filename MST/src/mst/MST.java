/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Kevin Veloso 11318626 e Richelieu Costa 11328634
 */
public class MST {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner input = null;
        try {
            input = new Scanner(new File("dij10.txt"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int rows = 0;
        int columns = 0;

        int num_vertices = input.nextInt();

        rows = columns = num_vertices;
        int[][] a = new int[rows][columns];

        for (int i = 0; i <= num_vertices - 1; ++i) {

            a[i][i] = 0;
            for (int j = i + 1; j <= num_vertices - 1; ++j) {

                a[i][j] = a[j][i] = input.nextInt();

            }
        }

        input.close();

        PRIM mst = new PRIM(num_vertices);

        printMatrix(a);
        mst.primMST(a);
    }
    
    public static void printMatrix(int a[][]) {

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println(" ");
        }

    }
    

}
