package mst;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class Heap {
    
    
    public static ArrayList<HeapNode> buildMinHeap(int[] vertices) {
        
        ArrayList<HeapNode> heap = new ArrayList<HeapNode>();
               
        for(int i = 0; i < vertices.length; i++) {
            HeapNode node = new HeapNode(i);
            node.setValue(vertices[i]);
            heap.add(node);
        }
        
        for(int i = 0; i < (heap.size() - 2); i++) {
            
            heap.get(i).setLtChild(heap.get((2*i)+1));
            heap.get(i).setRtChild(heap.get((2*i)+2));
            
            heap.get((2*i)+1).setParent(heap.get(i));
            heap.get((2*i)+2).setParent(heap.get(i));
            
        }
        
        return heap;
    }
    
       
    public static void swap(HeapNode a, HeapNode b) {
        HeapNode aux = new HeapNode();
        
        aux.setValue(a.getValue());
        a.setValue(b.getValue());
        b.setValue(aux.getValue());
    }
    
    public static void minHeapfy(HeapNode root) {
        
        HeapNode min = root;
                    
        if(!root.isLeaf()) {
            
            if(root.hasLtChild() && root.getLtChild().getValue() < min.getValue()) {
                min = root.getLtChild();
            }
            if(root.hasRtChild() && root.getRtChild().getValue() < min.getValue()) {
                min = root.getRtChild();
            }
                
            if(min.getValue() != root.getValue()) {
                swap(min,root);
                minHeapfy(min);
            }
        }

    }
    
    public static HeapNode extractMin(ArrayList<HeapNode> nodes) {
        
        if(nodes.isEmpty()) {
            return null;
        }
        
        HeapNode min = nodes.get(0);
        
        nodes.set(0, nodes.get(nodes.size()-1));
        nodes.remove(nodes.get(nodes.size()-1));
        
        minHeapfy(nodes.get(0));
        
        return min;        
    }
    
    
    


    
}
