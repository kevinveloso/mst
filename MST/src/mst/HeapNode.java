/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;


public class HeapNode {
   
    HeapNode ltChild, rtChild, parent;
    int value, position;
    
    public HeapNode() {
    }
    
    public HeapNode(int position) {
        this.position = position;
    }

    
    public HeapNode getParent() {
        return this.parent;
    }
        
    public HeapNode getLtChild() {
        return this.ltChild;
    }
    
    public HeapNode getRtChild() {
        return this.rtChild;
    }
    
    public int getValue() {
        return this.value;
    }
    
        
    public int getPosition() {
        return this.position;
    }    
    
    public void setParent(HeapNode parent) {
        this.parent = parent;
    }
        
    public void setLtChild(HeapNode ltChild) {
        this.ltChild = ltChild;
    }
    
    public void setRtChild(HeapNode rtChild) {
        this.rtChild = rtChild;
    }
            
    public void setValue(int newValue) {
        this.value = newValue;
    }
    
    public boolean hasLtChild() {
        return (this.ltChild != null);
    }
        
    public boolean hasRtChild() {
        return (this.rtChild != null);
    }
    
    public boolean isLeaf() {
        return (hasLtChild() || hasRtChild());
    }
    
    
}
